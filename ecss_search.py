def first_run():
    import openpyxl

    # For most up-to-date database of terms, look here https://ecss.nl/home/ecss-glossary-terms/
    file_loc = "ECSS-Definitions(export-from-DOORS-database-v0.7_20Sept2019).xlsx"
    wb = openpyxl.load_workbook(file_loc)
    ecss_sheet = wb.active

    # Get Column Numbers
    for i in range(1, ecss_sheet.max_column + 1):
        if ecss_sheet.cell(row=1, column=i).value == "Term":  # Term Column
            ecss_term_col = i
        elif ecss_sheet.cell(row=1, column=i).value == "Active of Superseded Standard":  # Status Column
            ecss_status_col = i
        elif ecss_sheet.cell(row=1, column=i).value == "ECSS Standard":  # ECSS Standard Column
            ecss_col = i
        elif ecss_sheet.cell(row=1, column=i).value == "Definition number":  # Definition Number Column
            ecss_dnumber_col = i
        elif ecss_sheet.cell(row=1, column=i).value == "Definition text":  # Definition Text Column
            ecss_dtext_col = i
        elif ecss_sheet.cell(row=1, column=i).value == "Note":  # Note Column
            ecss_note_col = i

    # Term Column Conditioning (make all terms lowercase)
    for row in range(1, ecss_sheet.max_row + 1):
        if ecss_sheet.cell(row=row, column=ecss_term_col).value.islower == 1:
            continue
        else:
            temp_value = ecss_sheet.cell(row=row, column=ecss_term_col).value
            temp_value = temp_value.lower()
            ecss_sheet.cell(row=row, column=ecss_term_col, value=temp_value)
    return ecss_sheet, ecss_term_col, ecss_status_col, ecss_col, ecss_dnumber_col, ecss_dtext_col, ecss_note_col


def search(term, ecss_sheet, ecss_term_col, ecss_status_col, ecss_col, ecss_dnumber_col, ecss_dtext_col, ecss_note_col):
    from fuzzywuzzy import fuzz
    from heapq import nlargest
    similar = 5

    # Searching for the term
    exact_results = 0
    ranking = [(1, 0)]
    for row in range(2, ecss_sheet.max_row + 1):
        current_score = fuzz.partial_ratio(term, ecss_sheet.cell(row=row, column=ecss_term_col).value)
        current_score = current_score - 10
        if ecss_sheet.cell(row=row, column=ecss_term_col).value == term:
            current_score = current_score + 10
        if ecss_sheet.cell(row=row, column=ecss_status_col).value == "Superseded":
            current_score = current_score - 30
        ranking.append([row, current_score])

    # Rank top options

    top = nlargest(similar, ranking, key=lambda e: e[1])

    # Present results
    if top[0][1] >= 100:
        definition = ecss_sheet.cell(row=top[0][0], column=ecss_term_col).value
        standard = ecss_sheet.cell(row=top[0][0], column=ecss_col).value
        dnumber = ecss_sheet.cell(row=top[0][0], column=ecss_dnumber_col).value
        description = ecss_sheet.cell(row=top[0][0], column=ecss_dtext_col).value.strip().replace("\n", " ")
        output = "An exact definition has been identified!\n\n"
        output += "| Term:                 | Definition:                    |\n"
        output += "|:--------------------|:---------------------------------|\n"
        output += ("| **`%s`**| **%s**| \n\n" % (definition, description))
        status = ecss_sheet.cell(row=top[0][0], column=ecss_status_col).value
        output += ("_The term `%s` appears in ECSS standard `%s` (%s Standard) and has definition number `%s`_\n\n" % (
            definition, standard, status, dnumber))
        if ecss_sheet.cell(row=top[0][0], column=ecss_note_col).value is None:
            output += ("_This definition does not include any comments/notes._ \n")
        elif 'NOTE' in ecss_sheet.cell(row=top[0][0], column=ecss_note_col).value:  # Add notes if existing
            note = ecss_sheet.cell(row=top[0][0], column=ecss_note_col).value
            output += "This definition includes the following comments/notes:\n"
            output += "```\n"
            output += note
            output += "\n```\n"
        output += "***\n"
        output += "The following similar terms have also been identified:\n\n"
        output += "| Term:|Description:|Appears in:| Status:|\n"
        output += "|:-----------|:-----------:|:-----------|:--------|\n"
        for i in range(1, similar):
            standard = ecss_sheet.cell(row=top[i][0], column=ecss_col).value
            definition = ecss_sheet.cell(row=top[i][0], column=ecss_term_col).value
            status = ecss_sheet.cell(row=top[i][0], column=ecss_status_col).value
            description = ecss_sheet.cell(row=top[i][0], column=ecss_dtext_col).value.strip().replace("\n", " ").replace("<",
                                                                                                               "(").replace(
                ">", ")")
            output += (
                    "|**`%s`**|![Definition](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4/Infobox_info_icon.svg/32px-Infobox_info_icon.svg.png \"%s\") |`%s`|%s Standard|\n" % (
                definition, description, standard, status))
        output += "_Hover over the icon for bonus definitions_"
    else:
        definition = ecss_sheet.cell(row=top[0][0], column=ecss_term_col).value
        standard = ecss_sheet.cell(row=top[0][0], column=ecss_col).value
        dnumber = ecss_sheet.cell(row=top[0][0], column=ecss_dnumber_col).value
        description = ecss_sheet.cell(row=top[0][0], column=ecss_dtext_col).value.strip().replace("\n", " ")
        output = "No exact results have been identified. The _closest result_ is the following:\n\n"
        output += "| Term:                 | Definition:                    |\n"
        output += "|:--------------------|:---------------------------------|\n"
        output += ("| **`%s`**| **%s**| \n\n" % (definition, description))
        output += ("_The term `%s` appears in ECSS standard `%s` and has definition number `%s`_\n\n" % (
            definition, standard, dnumber))
        if ecss_sheet.cell(row=top[0][0], column=ecss_note_col).value is None:
            output += "_This definition does not include any comments/notes._ \n"
        elif 'NOTE' in ecss_sheet.cell(row=top[0][0], column=ecss_note_col).value:  # Add notes if existing
            note = ecss_sheet.cell(row=top[0][0], column=ecss_note_col).value
            output += "This definition includes the following comments/notes:\n"
            output += "```\n"
            output += note
            output += "\n```\n"
        output += "***\n"
        output += "The following similar terms have also been identified:\n\n"
        output += "| Term:|Description:|Appears in:| Status:|\n"
        output += "|:-----------|:-----------:|:-----------|:--------|\n"
        for i in range(1, similar):
            standard = ecss_sheet.cell(row=top[i][0], column=ecss_col).value
            definition = ecss_sheet.cell(row=top[i][0], column=ecss_term_col).value
            status = ecss_sheet.cell(row=top[i][0], column=ecss_status_col).value
            description = ecss_sheet.cell(row=top[i][0], column=ecss_dtext_col).value.strip().replace("\n", " ").replace("<",
                                                                                                               "(").replace(
                ">", ")")
            output += (
                    "|**`%s`**|![Definition](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4"
                    "/Infobox_info_icon.svg/32px-Infobox_info_icon.svg.png \"%s\")|`%s`|%s Standard|\n" % (
                        definition, description, standard, status))
        output += "_Hover over the icon for bonus definitions_"

    return output
