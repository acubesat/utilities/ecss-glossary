from ecss_search import first_run as ecss_first_run, search as ecss_search

def initialize():
    ecss_sheet, ecss_term_col, ecss_status_col, ecss_col, ecss_dnumber_col, ecss_dtext_col, ecss_note_col = ecss_first_run()
    return ecss_sheet, ecss_term_col, ecss_status_col, ecss_col, ecss_dnumber_col, ecss_dtext_col, ecss_note_col

def lookup(term):
    term = term.lower()  # Make term lowercase to enhance searching capability
    if not term:
        output = "Give me something to search, please!"
        return output  # Return warning to user if output is empty

    # Search across all available resources

    result = ecss_search(term, ecss_sheet, ecss_term_col, ecss_status_col, ecss_col, ecss_dnumber_col, ecss_dtext_col, ecss_note_col)
    # ranking_asatacro

    # Sort top options

    # Condition and return results to user

    return result
