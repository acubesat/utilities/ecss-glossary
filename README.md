# ECSS Glossary

ECSS Glossary is a utility which returns the definitions of ECSS terms. Integrates with the [Mattermost](https://mattermost.com/) messaging platform. It operates using the latest ECSS Terms and Definitions DOORS export, which can be acquired from the official [ECSS website](https://ecss.nl/home/ecss-glossary-terms/). Note that only citizens from ESA member states will be able to download this file. The utility will match user requests to the closest term found in the database based on Levensthein distance.

## Installation

1. Run `pip install -r requirements.txt` to install all required packages.
2. Download the [`ECSS-Definitions(export-from-DOORS-database-v0.7_20Sept2019).xlsx`](https://ecss.nl/home/ecss-glossary-terms/) from the ECSS website. For newer versions, see troubleshooting.
3. Place the `.xlsx` file in the repository directory.
4. Run `ecss_http_server.py`. It will listen to HTTP requests at port `9395` by default.
5. Setup mattermost command, as explained below.

## Integrating with mattermost

Follow the standard procedure to create a new slash command, as described [here](https://docs.mattermost.com/developer/slash-commands.html) and below.

1. Go to **Main Menu > Integrations > Slash Commands**
2. Click **Add Slash Command**.
3. Setup the **Command Trigger Word**.
4. Ensure the following options are setup in the **Edit** window.

**Options:**

- Title: `Search ECSS definitions & terms`
- Command Trigger Word: `term` *(or your specified trigger word)*
- Request URL: `http://localhost:9395/` *(or your desired port or hosting location)*
- Request Method: `POST`
- Autocomplete: `TRUE`
- Autocomplete hint: `[phrase]`
- Autocomplete description: `Searches for a phrase in ECSS Glossary`


### Required packages

The following packages are required to run this utility:

1. `openpyxl`
2. `fuzzywuzzy`

The package `python-Levenshtein-wheels` is not required to run the utility, but if installed it will significantly increase searching speed, resulting in faster outputs for the user.

## Troubleshooting

1. Latest version of the DOORS export is `ECSS-Definitions(export-from-DOORS-database-v0.7_20Sept2019).xlsx`, if a newer version exists you should change the name of `file_loc` in `ecss_search.py`.
2. If the port `9395` is already in use, it can be changed from the `ecss_http_server.py` file to any desired port.

